# SAE 2.03 - Installation d'un service réseau

==========

Membres du groupe : CHARLERY Klara, CROISIER Thibault, DARQUES Charlie

# Présentation

Ce rapport a pour but de rendre compte des séances de travail que nous avons passé au cours des dernières semaines.

Nous avons autant travaillé la théorie avec des questions de culture générale et de vocabulaire sur la configuration de machine virtuelle et Debian en général, mais aussi la pratique en revenant sur des connaissances d'installation de machine et de configuration en ligne de commande.  
Le présent fichier a pour utilité de montrer comment convertir le rapport en un fichier HTML ou PDF.

# Utilisation

En ligne de commande, se placer dans le répertoire du fichier Markdown et exécuter la commande souhaitée :

**ATTENTION**

Il faut au préalable avoir les paquets **pandoc** et **texlive** d'installé.

Pour les installer :

- Sur une distribution basé sur debian : `sudo apt install pandoc texlive`
- Sur une distribution basé sur Arch : `sudo pacman -S pandoc texlive`

Convertir en HTML : `pandoc -s --toc -c styles.css --template template.html SAe2.03-RapportFinal.md -o SAe2.03-RapportFinal.html`

Convertir en PDF : `pandoc -N --variable "geometry=margin=1.2in" --variable mainfont="Palatino" --variable sansfont="Helvetica" --variable monofont="Menlo" --variable fontsize=12pt --variable colorlinks=true --variable version=2.0 SAe2.03-RapportFinal.md --toc -o SAe2.03-RapportFinal.pdf`